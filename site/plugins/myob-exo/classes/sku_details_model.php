<?php
if ( !class_exists( 'SkuDetailsModel' )){

	class SkuDetailsModel{
		private $tbl = "sku_details";
		
		function __construct(){
			
		}
		
		function create_table(){
			global $wpdb;
			
			$table_name = $wpdb->prefix . 'sku_details';
			$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
				  `sku` varchar(30) NOT NULL,
				  `warehouse` varchar(20) NOT NULL,
				  `qty` int(4) DEFAULT NULL,
				  `price` decimal(10,0) NOT NULL,
				  `description` text,
				  `lastupdated` timestamp NULL DEFAULT NULL,
				  `soldcount` int(5) DEFAULT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
				$altersql = "ALTER TABLE ".$table_name." ADD PRIMARY KEY( `sku`, `warehouse`);";
				

			$wpdb->query($sql);
			$wpdb->query($altersql);
			
		}
		
		function add_item($sku, $kv){
			global $wpdb;
			
			$lastupdated = time();
			
			if(empty($sku)) throw new Exception('Cannot Add Item. SKU must be present');
			
			$item = $this->get_item($sku);
			if($item) throw new Exception('SKU '.$sku.' already exists.');
			
			$sql = "INSERT INTO {$wpdb->prefix}sku_details (`warehouse`, `qty`, `price`, `lastupdated`, `sku`)
			VALUES('{$kv['warehouse']}', '{$kv['qty']}', '{$kv['price']}', '{$lastupdated}', '{$sku}')";
			$wpdb->query($sql);
			
			return "success";
	
		}
		
		
		function get_item($sku){
			global $wpdb;
			
			if(empty($sku)) return false;
			$sql = "SELECT * FROM ".$wpdb->prefix."sku_details WHERE sku = '".$sku."'";
			
			$ret = $wpdb->get_row($sql);
			
			return $ret;
			
		}
		
		function update_item($sku, $kv){
			global $wpdb;
			
			$lastupdated = time();
			
			$item = $this->get_item($sku);
			if(!$item) $this->add_item($sku, $kv);
			
			$sql = "UPDATE {$wpdb->prefix}sku_details SET  warehouse= '{$kv['warehouse']}', qty= '{$kv['qty']}', price= '{$kv['price']}', lastupdated= '{$lastupdated}'	WHERE `sku`= '{$sku}'";
			$wpdb->query($sql);
			
			return "success";
			
		}
		
		function delete_item($sku){
			global $wpdb;
			
			$item = $this->get_item($sku);
			if(!$item) throw new Exception('sku '.$sku.' does not exist.');
			
			$sql = "DELETE FROM {$wpdb->prefix}sku_details
			WHERE `sku`= '{$sku}'";
			$wpdb->query($sql);
			
			return "success";
			
		}
		
		function get_sku_list_oldupdated($num = 500){
			
			global $wpdb;
			
			$sql = "SELECT * FROM ".$wpdb->prefix."sku_details ORDER BY lastupdated ASC limit ".$num."";
			
			$ret = $wpdb->get_results($sql);
			
			return $ret;
		}
		
		function get_all_skus() {
			
			global $wpdb;
			
			$sql = "SELECT * FROM ".$wpdb->prefix."sku_details";
			
			$ret = $wpdb->get_results($sql);
			
			return $ret;
			
		}
		
		
	} //class ends
	
} //if class ends