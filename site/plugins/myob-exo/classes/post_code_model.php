<?php
if ( !class_exists( 'PostCodeModel' )){

	class PostCodeModel{
		
		function __construct(){
			
		}
		
		function create_table(){
			global $wpdb;
				
			$sql = "CREATE TABLE IF NOT EXISTS  ".$wpdb->prefix. "post_code ( ";
			$sql .= " postcode int(4) NOT NULL PRIMARY KEY,  ";
			$sql .= " metro int(1) DEFAULT NULL, ";
			$sql .= " warehouse varchar(3) DEFAULT NULL ";
			$sql .= ") ENGINE = InnoDB;" ;
			
			$wpdb->query($sql);
			
		}
		
		
	} //class ends
	
} //if class ends