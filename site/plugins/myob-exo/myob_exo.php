<?php

/**
 * Plugin Name: Myob Exo.
 * Plugin URI: 
 * Description: Myob Exo.
 * Version: 6.3
 * Author: 
 * Author URI: 
 */
//error_reporting(E_ALL);
//ini_set('display_errors', 1); 
require_once(__DIR__ ."/classes/post_code_model.php");
//require_once(__DIR__ ."/classes/sku_details_model.php");
require_once(__DIR__ ."/classes/abnlookupclass.php");
 
if ( !class_exists('MyobExo')){
	
	class MyobExo{
		
		
		function __construct(){
			
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') );
			//add_action('woocommerce_before_single_product', array(&$this, 'current_product') );
			//add_action('plugins_loaded', array(&$this, 'plugins_loaded') );
			//add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') ); 
			add_action('init', array(&$this, 'init'));
			add_shortcode( 'postcode_popup', array(&$this,'aspk_popup') );
			add_shortcode( 'postcode_link_update', array(&$this,'postcode_link_update') );
			add_action('admin_menu', array(&$this,'admin_menu'));	
			add_action('wp_head', array(&$this,'wp_head'));	
			add_action('wp_login', array(&$this,'wp_login'));	

			add_action( 'wp_ajax_aspk_supd', array(&$this, 'stock_update'));
			add_action( 'wp_ajax_nopriv_aspk_supd', array(&$this, 'stock_update') );

			add_action( 'woocommerce_after_order_notes' , array(&$this,'woocommerce_after_order_notes') );			
			add_filter( 'woocommerce_checkout_fields' , array(&$this,'add_abn') );
			add_action( 'woocommerce_checkout_update_order_meta' , array(&$this,'save_abn'), 10, 2 );
			add_action( 'woocommerce_view_order', array(&$this,'display_order_data'), 20 );
			add_action( 'woocommerce_thankyou', array(&$this,'display_order_data'), 20 );
			add_action( 'woocommerce_admin_order_data_after_order_details', array(&$this,'display_order_data_in_admin') );
			add_action( 'woocommerce_process_shop_order_meta', array(&$this,'save_abn_details'), 45, 2 );
			add_action('woocommerce_email_customer_details', array(&$this,'show_email_order_meta'), 30, 3 );
			add_action( 'wp_ajax_aspk_lookup', array(&$this, 'abn_lookup'));
			add_action( 'wp_ajax_nopriv_aspk_lookup', array(&$this, 'abn_lookup') );
			add_action( 'wp_footer', array(&$this,'jsabn') ); 
			add_action( 'woocommerce_order_status_processing',array(&$this, 'order_status_changed'));
			add_action( 'woocommerce_product_query',array(&$this, 'woocommerce_product_query'));
			
			add_filter( 'woocommerce_email_recipient_new_order' , array(&$this,'woocommerce_email_recipient'), 10, 2 );
			add_filter( 'woocommerce_email_recipient_failed_order' , array(&$this,'woocommerce_email_recipient'), 10, 2 );
			add_filter( 'woocommerce_email_recipient_cancelled_order' , array(&$this,'woocommerce_email_recipient'), 10, 2 );

		}
		
		
		function wp_login(){
				unset($_SESSION['post_data']);
				unset($_SESSION['out_of_coverage_area']);
				unset($_SESSION['correct_postcode']);
		}
		
		function woocommerce_email_recipient( $recipient, $order){
			// Bail on WC settings pages since the order object isn't yet set yet
			// Not sure why this is even a thing, but shikata ga nai
			$page = $_GET['page'] = isset( $_GET['page'] ) ? $_GET['page'] : '';
			if ( 'wc-settings' === $page ) {
				return $recipient; 
			}
			
			// just in case
			/* if ( ! $order instanceof WC_Order ) {
				return $recipient; 
			} */
			
			$items = $order->get_items();
			foreach($items as $item){

				$prod = $order->get_product_from_item( $item );
				if(! $prod) continue;
				
				$sku = $prod->get_sku();
				if(! $sku) continue;

				if(stristr($sku, 'THOM') !== FALSE){
					$recipient .= ',T360Melbourne@Tyremax.com.au';
					return $recipient;
				}elseif(stristr($sku, 'ERK') !== FALSE){
					$recipient .= ',T360Sydney@Tyremax.com.au';
					return $recipient;
				}elseif(stristr($sku, 'ROCK') !== FALSE){
					$recipient .= ',T360Brisbane@Tyremax.com.au';
					return $recipient;
				}
				break;
				
			}
			
			return $recipient;

		}
		
		function install() { 
			
			$postCodeModel = new PostCodeModel();
			//$skuDetailsModel = new SkuDetailsModel();
			
			$postCodeModel->create_table();
			//$skuDetailsModel->create_table();
			
		}
		
		function wp_enqueue_scripts(){
			
			wp_enqueue_script('jquery');
			wp_enqueue_script('bootstrap-js',plugins_url('js/bootstrap.js', __FILE__));
			wp_enqueue_style( 'bootstrap', plugins_url('css/bootstrap.css', __FILE__) );
			wp_enqueue_style( 'aspk', plugins_url('css/aspk.css', __FILE__) );
		}
		
		function jsabn(){
			if(isset($_SESSION['post_data'])){
				$json_array = json_decode($_SESSION['post_data']);
				$pc = $json_array->postcode;
			}
		?>
		<div id="invalidabnmodal" class="modal fade" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Invalid ABN</h4>
					</div>
					<div class="modal-body">
					  <p id="abn-message">Please enter a valid ABN</p>
					</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
				
		<script>
			var abnst = "Not Active";

			jQuery(window).load(function(){
				if(abnst == "Active"){
					jQuery('#place_order').show();
				}else{
					jQuery('#place_order').hide();
				}
				
			});
			
			jQuery(document).ready(function(){
				jQuery('#place_order').hide();
				jQuery('#abnstatus').attr('readonly', true);
				jQuery('#abnstatus').css('background', 'rgba(211, 211, 211, 0.37)');
				jQuery('#companyname').attr('readonly', true);
				jQuery('#companyname').css('background', 'rgba(211, 211, 211, 0.37)');
				
				<?php if(is_checkout()){ ?>
				jQuery('#billing_postcode').attr('readonly', true);
				jQuery('#billing_postcode').css('background', 'rgba(211, 211, 211, 0.37)');
				<?php } ?>
				
				jQuery('#abnid').on('focusout', function() {
					var x = jQuery('#abnid').val();

					if(x !== '') {
						jQuery('#abnstatus').val('');
						jQuery('#companyname').val('');
						
						jQuery.get("/wp-admin/admin-ajax.php?action=aspk_lookup"+"&abn="+x , function (resData) {
							
							if (resData) {
								if(resData == 'invalid') {
									jQuery('#abn-message').text('Please enter a valid ABN.');
									jQuery('#invalidabnmodal').modal();
									jQuery('#abnstatus').val('');
									jQuery('#companyname').val('');
									jQuery('#place_order').hide();
								}else {
									var res = JSON.parse(resData);
									
									jQuery('#abnstatus').val(res.abnstatus);
									jQuery('#companyname').val(res.cmpName);
									abnst = jQuery('#abnstatus').val();
									abnst = abnst.toString().trim();
									if(abnst == "Active"){
										jQuery('#place_order').show();
									}else{
										jQuery('#place_order').hide();
										jQuery('#abn-message').text('ABN is not active. An active ABN is required to purchase.');
										jQuery('#invalidabnmodal').modal();
									}
									
								}
							}
						});
					}
				});
				
				jQuery('#abnid').on('change', function() {
					jQuery('#place_order').hide();
				});
				
			});
		</script> 
		<?php	
		
		}
		
		function woocommerce_after_order_notes($checkout){
			echo '<div id="d_disclaimer"><h3>'.__(' ').'</h3>';
 
			woocommerce_form_field( 'ddisclaimer', array(
				'type'          => 'checkbox',
				'class'         => array('input-checkbox'),
				'label'         => __('Leave tyres at my premises if I am not present at the time of delivery. I have read and agree to the <a href="/delivery-disclaimer/" target="_blank">delivery disclaimer</a> (click to read disclaimer).'),
				'required'  => false,
				), 'yes');
		 
			echo '</div>';
			
		}
		
		function abn_lookup(){
			
			$abn = $_GET['abn'];
			
			if(empty($abn)){
				echo 'invalid';
				exit;
			}
			
			$validAbn = $this->isValidAbn($abn);
		
			if(! $validAbn){
				echo 'invalid';
				exit; 
			} 
		
			$guid = "56418b9a-ddd6-48cf-8fcd-dc14574d75c4";
			try {
				
				$aspkLookup = new abnlookup($guid);
				
				try{
					
					$ret = $aspkLookup->searchByAbn($abn);
					$res = array();

					if($ret){
						$res['abnstatus'] = $ret->ABRPayloadSearchResults->response->businessEntity->entityStatus->entityStatusCode;
						$res['cmpName'] = $ret->ABRPayloadSearchResults->response->businessEntity->mainName->organisationName;
					}
					
					echo json_encode($res) ;
					
				} catch(Exception $e){
					throw $e;
				}
				
			} catch(Exception $e){
				echo $e->getMessage();
			}
			
			exit;
			
		}
		
		
		public static function isValidAbn($abn) {
			  
			$weights = array(10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19);
			// Strip non-numbers from the acn
			$abn = preg_replace('/[^0-9]/', '', $abn);
			// Check abn is 11 chars long
			if(strlen($abn) != 11) {
			  return false;
			}
			// Subtract one from first digit
			$abn[0] = ((int)$abn[0] - 1);
			// Sum the products
			$sum = 0;
			foreach(str_split($abn) as $key => $digit) {
			  $sum += ($digit * $weights[$key]);
			}
			if(($sum % 89) != 0) {
			  return false;
			}
			return true;
		}
		
		function add_abn($fields) {
			
			$fields['billing']['billing_abn'] = array(
				'label'     => __('ABN', 'woocommerce'),
				'placeholder'   => _x('ABN', 'placeholder', 'woocommerce'),
				'required'  => true,
				'class'     => array('form-row-first'),
				'id'     => 'abnid',
				'onfocusout'     => array('jsabn()'),
				'clear'     => true
			);
			
			$fields['billing']['billing_abn_status'] = array(
				'label'     => __('ABN Status', 'woocommerce'),
				'class'     => array('form-row-last'),
				'id'     => 'abnstatus',
				'clear'     => true
			);
			
			$fields['billing']['billing_company'] = array(
				'label'     => __('Company name', 'woocommerce'),
				'id'     => 'companyname',
				'clear'     => true
			);
			
			$order = array(
				"billing_first_name", 
				"billing_last_name", 
				"billing_abn", 
				"billing_abn_status", 
				"billing_company", 
				"billing_address_1", 
				"billing_address_2", 
				"billing_city",
				"billing_postcode", 
				"billing_state",
				"billing_country",
				"billing_phone",
				"billing_email"
			);
			
			foreach( $order as $field ) {
				$ordered_fields[$field] = $fields["billing"][$field];
			}

			$fields["billing"] = $ordered_fields;

			return $fields;
	 
		}
		
		function save_abn($order_id, $posted){
			
			// don't forget appropriate sanitization if you are using a different field type
			if( isset( $posted['billing_abn'] ) ) {
				update_post_meta( $order_id, '_billing_abn', sanitize_text_field( $posted['billing_abn'] ) );
			}
			
			if( isset( $posted['billing_abn_status'] ) ) {
				update_post_meta( $order_id, '_billing_abn_status', sanitize_text_field( $posted['billing_abn_status'] ) );
			}
			
			if( array_key_exists('ddisclaimer', $_POST) ) {
				update_post_meta( $order_id, '_d_disclaimer', 'yes' );
			}
			//update_post_meta( $order_id, '_d_disclaimer', 'yes' );
			
		}
		
		function display_order_data( $order_id ){  ?>
			<table class="woocommerce-table woocommerce-table--customer-details shop_table customer_details">
				<tbody>
					<tr>
						<th><?php _e( 'Delivery Disclaimer:' ); ?></th>
						<td><?php if(empty(get_post_meta( $order_id, '_d_disclaimer', true ))){
							echo "I don't accept";
						}else{
							echo "I accept";
						}	
						?></td>
					</tr>
					<tr>
						<th><?php _e( 'ABN:' ); ?></th>
						<td><?php echo get_post_meta( $order_id, '_billing_abn', true ); ?></td>
					</tr>
					<tr>
						<th><?php _e( 'ABN Status:' ); ?></th>
						<td><?php echo get_post_meta( $order_id, '_billing_abn_status', true ); ?></td>
					</tr>
				</tbody>
			</table>
		<?php }
		
		function display_order_data_in_admin( $order ){  ?>
			<div class="order_data_column">

				<h4><?php _e( 'Additional Information', 'woocommerce' ); ?><a href="#" class="edit_address"><?php _e( 'Edit', 'woocommerce' ); ?></a></h4>
				<div class="address">
				<?php
					echo '<p><strong>' . __( 'Delivery Disclaimer' ) . ':</strong></p>';
					if(empty(get_post_meta($order->id, '_d_disclaimer', true ))){
						echo "I don't accept";
					}else{
						echo "I accept";
					}	
					echo '<p><strong>' . __( 'ABN' ) . ':</strong>' . get_post_meta( $order->id, '_billing_abn', true ) . '</p>'; 
					echo '<p><strong>' . __( 'ABN Status' ) . ':</strong>' . get_post_meta( $order->id, '_billing_abn_status', true ) . '</p>'; ?>
				</div>
				<div class="edit_address">
					<?php 
					 woocommerce_wp_text_input( array( 'id' => '_billing_abn', 'label' => __( 'ABN' ), 'wrapper_class' => '_billing_abn' ) );
					 woocommerce_wp_text_input( array( 'id' => '_billing_abn_status', 'label' => __( 'ABN Status' ), 'wrapper_class' => '_billing_abn_status' ) ); 
					 ?>
				</div>
			</div>
		<?php }
		
		function save_abn_details( $post_id, $post ){
			update_post_meta( $post_id, '_billing_abn', wc_clean( $_POST[ '_billing_abn' ] ) );
			update_post_meta( $post_id, '_billing_abn_status', wc_clean( $_POST[ '_billing_abn_status' ] ) );
		}
		
		function show_email_order_meta( $order, $sent_to_admin, $plain_text ) {
			$abn = get_post_meta( $order->id, '_billing_abn', true );
			$abnStatus = get_post_meta( $order->id, '_billing_abn_status', true );
			
			if( $plain_text ){
				echo $abn;
				echo $abnStatus;
				if(empty(get_post_meta($order->id, '_d_disclaimer', true ))){
					echo  __( 'Delivery Disclaimer' ) . ": I don't accept.";
				}else{
					echo __( 'Delivery Disclaimer' ) . ": I  accept.";
				}
			} else {
				echo '<p><strong>ABN</strong>: ' . $abn."</p>";
				echo '<p><strong>ABN Status</strong>: ' . $abnStatus."</p>";
				if(empty(get_post_meta($order->id, '_d_disclaimer', true ))){
					echo '<p><strong>' . __( 'Delivery Disclaimer' ) . ':</strong>'."I don't accept </p>";
				}else{
					echo '<p><strong>' . __( 'Delivery Disclaimer' ) . ':</strong>'."I accept </p>";
				}	
			}
		}
		
		function postcode_link_update(){
			if(isset($_SESSION['post_data'])){
				$json_array = json_decode($_SESSION['post_data']);
				$pc = $json_array->postcode;
		
			?>
			<div class="jig-valid-postcode-msg">
				<p>Showing products available to postcode:  <?php echo $pc ?> (<a href="<?php echo "/shop?aspkresetpc=reset"; ?>"><u>change</u></a>) </p>
			</div>
			<?php
			
		}
			if(isset($_SESSION['out_of_coverage_area'])){
				$out_of_coverage_area = $_SESSION['out_of_coverage_area'];
		
			?>
			<div class="jig-out-of-coverage-area-postcode-black-msg">
				<p>Showing products available to postcode:  <?php echo $out_of_coverage_area ?> (<a href="<?php echo "/shop?aspkresetpc=reset"; ?>"><u>change</u></a>) </p>
			</div>
			<div class="jig-out-of-coverage-area-postcode-red-msg">
				<font color="red">Sorry, we don't currently deliver to this area.</font> <br>
				<font color="red">To enquire about a special delivery <a href="/contact/"><u> <font color="red">contact us </font></u></a>. </font>
			</div>
			<?php
			
			}
			
			
		}
		
		function init(){
			
			if(! session_id() ) session_start();
			ob_start();
			
			
		}
		
		function wp_head(){
			if(is_singular( 'product' )){
				
				if(isset($_SESSION['correct_postcode'])) {
					ob_end_flush();
					return;
				}
				
				ob_end_clean();
				wp_redirect( home_url() );
				exit;
			}
			ob_end_flush();
		} 
		
		function getWarehouse($postcode){
			
			global $post;
			
			$kv = array();
			
			$args = array(
			'posts_per_page'   => 2000,
			'post_type'        => 'distributionarea',
			's'   => $postcode
			
			);
			$posts_array = get_posts( $args ); 
			foreach($posts_array as $post){
				$id = $post->ID;
				$title = $post->post_title;
				$postWarehouse = get_post_meta( $id,'warehouse' );
				$metroRegion = get_post_meta( $id,'MetroRegion' );
				
				$kv['warehouse'] = $postWarehouse;
				$kv['MetroRegion'] = $metroRegion;
				$kv['postcode'] = $title;
				
				if($postcode === $title) break;
				
			}
			
			return $kv;
					
		}
		
		function aspk_popup(){
	 
				if(isset($_GET['aspkresetpc'])){
					unset($_SESSION['post_data']);
					unset($_SESSION['out_of_coverage_area']);
					unset($_SESSION['correct_postcode']);
					
				}
				 
				if(isset($_SESSION['post_data'])) return;
				
				if(isset($_SESSION['out_of_coverage_area'])) return;
				
				$uid = get_current_user_id();
				if($uid > 0 && isset($_GET['aspkresetpc'])){
					if(isset($_POST['txtPostCode'])){
						update_user_meta($uid, 'billing_postcode', $_POST['txtPostCode']);
					}
				}
				
				if($uid > 0 && !isset($_GET['aspkresetpc'])){
					$mpc = get_user_meta($uid,  'billing_postcode', true );
					$_POST['txtPostCode'] = $mpc;
				}
				
			if(isset($_POST['txtPostCode'])){

					$postcode = $_POST['txtPostCode'];
					$aspkPost = $this->getWarehouse($postcode);
					if(empty($aspkPost))
						{	
							unset($_SESSION['correct_postcode']);
							$_SESSION['out_of_coverage_area'] = $postcode;
							
							return;
						}
					
					$post_warehouse = $aspkPost['warehouse'];
					$post_metro_region= $aspkPost['MetroRegion'];
					$post_id = $aspkPost['postcode'];
						
					if($postcode && $post_id){
						if($postcode != $post_id){
							
							unset($_SESSION['correct_postcode']);
							$_SESSION['out_of_coverage_area'] = $postcode;
								
							return;
						} 
					}
					
							$correctpc =  "correctPostcode";
							$_SESSION['correct_postcode'] = $correctpc;
							$json_array = json_encode($aspkPost); 
							$_SESSION['post_data'] = $json_array;
							
						
							
							?>
								<script>
									window.location.href='<?php echo strtok($_SERVER["REQUEST_URI"],'?');?>';
								</script>
							<?php
						
			}else{
					?>
						
							<div class="jig-postcode-main">
								<p class="jig-postcode-msg">Set your delivery postcode.</p>
								<form name="dontcare"  method="POST">
									<table>
											<tr>
												<td> 
													<div class="jig-postcode-fg">
														<input type="text" name="txtPostCode" class="form-control jig-postcode-fc"  placeholder="Delivery Postcode">
													</div>
												</td> 
												<td>
												<div  style="margin-left:10px" ><button type="submit" name="saveTxtPost" class="btn btn-primary jig-postcode-btn-primary" >SET</button></div>
												</td>
											</tr>
										
									</table>
												
								</form>
							</div>
					<?php
					 	
				}
		
				
			
		}
		
		
		
		function get_product_info_from_myob($response,$sku){
				
				$product_arr = array();
				$product_info_arr = array();
				$price = 0;
				
				if(isset($_SESSION['post_data'])){
					$json_array = json_decode($_SESSION['post_data']);
					$wh = $json_array->warehouse;
					
					 foreach($wh as $k => $v) {
						 $whouse = $v;
					 }
					 
					$salePrices = $response->saleprices;
					foreach ($salePrices as $k => $v){
					
						$id = $v->id;
						if($id == "16") {
							$price = (string) round($v->price, 2);
							break;
						}
					}
				 
					$qty = $response->totalinstock; //Will update according to warehouse
					foreach ($response->stocklevels as $sl) {
						
						switch($sl->locationid){
							case "1":
								if(strtolower($whouse) == 'thom') $qty = $sl->free;
								break;
							case "3":
								if(strtolower($whouse) == 'erk') $qty = $sl->free;
								break;
							case "4":
								if(strtolower($whouse) == 'rock') $qty = $sl->free;
								break;
						}
					}
					
					$obj = new stdClass();
					
					$obj->price = $price;
					$obj->quantity = $qty;
					
					$product_arr[$sku] = $obj; //TODO CHANGE SKU
					
					$product_info_arr['warehouse'] = $whouse;
					$product_info_arr['product_arr'] = $product_arr;
				}
				return $product_info_arr;
				
		}
		
		
		function current_product($response , $sku){
			
			global $product;
			
			if($product->is_virtual()) return;
			
			if(! $product->managing_stock()) return;
			
			$kv = array();
			
			$prod_arr = $this->get_product_info_from_myob(); 
			
			//$sdm = new SkuDetailsModel();
			
			try{
				if($prod_arr){
					$warehouse = $prod_arr['warehouse'];
					$kv['warehouse'] = $warehouse;
					
					$prodArray = $prod_arr['product_arr'];
				
					foreach($prodArray as $k => $v) {
					
						$myobSku = $k;
						$myobPrice = $v->price;
						$myobQty = $v->quantity;
						
						$kv['price']= $v->price;
						$kv['qty']= $v->quantity;
						
						//$sdm->update_item($myobSku,$kv);
						$product->set_price($myobPrice);
						$product->set_stock_quantity($myobQty);
						
					}
				}
			}
			catch(Exception $ex){
				file_put_contents(__DIR__ .'/kdebug', __FILE__ .' '. __LINE__ .' '. print_r($ex, true) . PHP_EOL, FILE_APPEND);//TODO
			}
	
			$sku = $product->get_sku();
			//$item = $sdm->get_item($sku);
			
			if($item){
				$tbl_sku = $item->sku;
				$tbl_price = $item->price;
				$tbl_qty = $item->qty;
				
				
				if($sku != $tbl_sku) exit;
				
				$product->set_price($tbl_price);
				$product->set_stock_quantity($tbl_qty);
			}
			
			
			
			//file_put_contents(__DIR__ .'/kdebug', __FILE__ .' '. __LINE__ .' '. print_r($quantity, true) . PHP_EOL, FILE_APPEND);//TODO
			return $product;
		}
				
		
		function admin_menu(){
			
			add_menu_page('Aspk Myob', 'Aspk Myob', 'manage_options', 'aspk_myob', array(&$this, 'aspk_myob_func'));
			
			
		}
		
		function aspk_myob_func(){
			
			if(isset($_POST['aspkSaveSettings'])) {
				
				$username = trim($_POST['username']);
				$password = trim($_POST['password']);
				$apiKey = trim($_POST['apiKey']);
				$apiHost = trim($_POST['apiHost']);
				$accessToken = trim($_POST['accessToken']);
				
				$authHash = "Basic ". base64_encode($username . ':' . $password);
				
				update_option( 'aspk_exo_username', $username );
				update_option( 'aspk_password', $password );
				update_option( 'aspk_exo_password', $authHash );
				update_option( 'aspk_exo_apiKey', $apiKey );
				update_option( 'aspk_exo_apiHost', $apiHost );
				update_option( 'aspk_exo_accessToken', $accessToken );
				
			}
			
			?>
				<form name="dontcare"  method="POST">
					<table style="width:40%">
						
							<th> <h3>Settings For My API Integration </h3> </th>
						
						<br>
						<tr>
							<td> Username: </td> 
							<td><input type="text" name="username" value= "<?php echo get_option( 'aspk_exo_username' ); ?>"></td>
						</tr>
						
						<tr>
							<td> Password: </td> 
							<td> <input type="password" name="password" value= "<?php echo get_option( 'aspk_password' ); ?>"></td>
						</tr>
						<tr>
							<td>Api-host: </td>
							<td><input type="text" name="apiHost" value= "<?php echo get_option( 'aspk_exo_apiHost' ); ?>"></td>
						</tr>
						<tr>
							<td>Api-key: </td> 
							<td><input type="text" name="apiKey" value= "<?php echo get_option( 'aspk_exo_apiKey' ); ?>"></td>
						</tr>
						<tr>
							<td>Access Token:</td> 
							<td> <input type="text" name="accessToken" value= "<?php echo get_option( 'aspk_exo_accessToken' ); ?>"></td>
						</tr>
						<tr>
							<td><button type="submit" name="aspkSaveSettings" class="btn btn-primary" >Save Settings</button></td>
						</tr>
					</table>
											
					
				</form>
			<?php
		} 
		
		function stock_update(){
			$skip_ids = array();

			$prod_id_list = $this->get_prod_id_list();

			
			if(!$prod_id_list) {
				$msg = "no prod list found.";
				$this->aspk_log($msg);
				exit;
			} 
		
			foreach ($prod_id_list as $pid){
				
				if (array_key_exists(strval($pid), $skip_ids)) continue;

				$p = wc_get_product($pid);
				if(! $p) continue;

				$sku = $p->get_sku();
				if(! $sku) continue;
				
				$sku = str_ireplace('THOM-', '', $sku);
				$sku = str_ireplace('ERK-', '', $sku);
				$sku = str_ireplace('ROCK-', '', $sku);

				$response = $this->update_prod_from_exo($sku);

				if($response){ 
					
					$price = 0;
					$thom = 0;
					$erk = 0;
					$rock = 0;

					foreach ($response->saleprices as $sp) {
						
						if($sp->id == 16){
							$price = $sp->price;
							break;
						}
					}

					foreach ($response->stocklevels as $sl) {
						
						switch($sl->locationid){
							case "1":
								$thom = $sl->free;
								break;
							case "3":
								$erk = $sl->free;
								break;
							case "4":
								$rock = $sl->free;
								break;
						}
					}

					$pid = $this->update_product('THOM-'.$sku, $thom, $price);
					$skip_ids[$pid] = 1;
					$pid = $this->update_product('ERK-'.$sku, $erk, $price);
					$skip_ids[$pid] = 1;
					$pid = $this->update_product('ROCK-'.$sku, $rock, $price);
					$skip_ids[$pid] = 1;
		
				}

			} 
			
			exit;
		}

		function update_product($sku, $qty, $price){

			$pid = wc_get_product_id_by_sku($sku);
			if($pid == 0 ) return;
			
			$p = wc_get_product($pid);
			if(! $p) return;

			$p->set_regular_price(strval($price));
			$p->set_manage_stock(true);
			$p->set_stock_quantity(intval($qty));
			$p->set_date_modified(time());
			$p->save();
			echo $pid."<br/>";//todo

			return strval($pid);

		}

		function update_prod_from_exo($sku){

			$host = "https://exo.api.myob.com/stockitem/".$sku;
			
			$authHash = get_option( 'aspk_exo_password' );
			
			$apiKey = get_option( 'aspk_exo_apiKey' );
			
			$accessToken = get_option( 'aspk_exo_accessToken' );
			
			$response = $this->sendPost($host,$authHash,$apiKey,$accessToken);
		
			return $response; 

		}
		
		function get_prod_id_list(){
			global $wpdb;

			$sql = "SELECT ID FROM ". $wpdb->prefix . "posts WHERE post_type='product' AND post_status='publish' order by post_modified ASC limit 25 ";
			
			$ret = $wpdb->get_col($sql);
			
			return $ret;
		}
		
		
		function aspk_log($err){
			file_put_contents(ABSPATH.'uploads/aspk_exo_error_log', __FILE__ .' '. __LINE__ .' '. print_r($err, true) . PHP_EOL, FILE_APPEND);
		}
		
		function sendPost($host,$authHash,$apiKey,$accessToken, $kv=array()){

			$headers[] = 'Authorization: '.$authHash;
			$headers[] = 'Accept: application/json';
			$headers[] = 'x-myobapi-key: '.$apiKey;
			$headers[] = 'x-myobapi-exotoken: '.$accessToken;
			
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $host);
			if(! empty($kv)){
				echo"in empty kv";
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $kv ));
			}
			
			curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec ($ch);

			if($server_output === false){
				
				print_r(curl_error($ch));
				return false;
				
			}
			curl_close ($ch);
			
			return json_decode($server_output);
		}

		function order_status_changed($order_id ){
			global $woocommerce;
			
			$order = new WC_Order($order_id);
			$total = $order->get_total();
			$user_id = $order->user_id;
			$items = $order->get_items();
			
			$f_name = get_user_meta( $user_id, 'billing_first_name', true );
			$l_name = get_user_meta( $user_id, 'billing_last_name', true );
			

			//$api_user_id = $this->get_user_from_api($f_name, $l_name);
			$api_user_id = $this->getMyobDebitorFromItem($items);
			
			
			
			$user_name = $f_name.' '.$l_name;
			$user_email = get_user_meta( $user_id, 'billing_email', true );
			$user_zip = get_user_meta( $user_id, 'billing_postcode', true );
			$user_phone = get_user_meta( $user_id, 'billing_phone', true );
			$company_name = get_user_meta( $user_id, 'billing_company', true );
			
			$deliveryaddress = array();
			$deliveryaddress['line1'] = get_user_meta( $user_id, 'billing_address_1', true );
			$deliveryaddress['line2'] = get_user_meta( $user_id, 'billing_address_2', true );
			$deliveryaddress['line3'] = get_user_meta( $user_id, 'billing_city', true );
			$deliveryaddress['line4'] =	$user_zip;
			$deliveryaddress['line5'] = get_user_meta( $user_id, 'billing_state', true );
			$deliveryaddress['line6'] = get_user_meta( $user_id, 'billing_country', true );
			
				
			if($api_user_id == '0'){
				
				$api_user_id = $this->create_user_via_api($user_name, $user_email, $user_zip, $user_phone, $deliveryaddress, $company_name);
				
				$order->add_order_note("Created customer on EXO with id ".$api_user_id);
				
			}else{
				$order->add_order_note("Customer exists on EXO with id ".$api_user_id);
			}
			
			$api_order_id = $this->create_order_via_api($api_user_id, $order_id, $items, $total, $deliveryaddress);
			
		}

		function create_user_via_api($user_name, $user_email, $user_zip, $user_phone, $deliveryaddress, $company_name = ''){
			$user_uri = "https://exo.api.myob.com";
			$url = $user_uri.'/debtor/';
						
			$headers = array();
			$headers['Authorization'] = get_option( 'aspk_exo_password' );
			$headers['x-myobapi-key'] = get_option( 'aspk_exo_apiKey' );
			$headers['x-myobapi-exotoken'] = get_option( 'aspk_exo_accessToken' );
			$headers['Accept'] = 'application/json';
			$headers['Content-Type'] = 'application/json';
			
			$dll = array();
			$extra = array();
			
			$dll['key'] =  'x_HasDOLLogin';
			$dll['value'] =  'N';
			
			$extra[] = $dll;
			
			
			$body = array(
				'accountname' => $user_name,
				'email' => $user_email,
				'phone' => $user_phone,
				'postalcode' => $user_zip,
				'website' => home_url(),
				'defaultcompanyname' => $company_name,
				'deliveryaddress' => $deliveryaddress,
				'extrafields' => $extra,
				'primarygroupid' => 0
			);
						
			$args = array(
				'method' => 'POST',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => $headers,
				'body' => json_encode($body),
				'cookies' => array()
			);
			
			$response = wp_remote_post( $url, $args);
			
			$body = $response['body'];
			$body_obj = json_decode($body);
			
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   $this->aspk_log($error_message);
			   $retarr = '0';
			   return $retarr;
			   
			}else{
				
				if(stristr($response['body'], 'Error')){
					$this->aspk_log($response);
					$retarr = '0';
					return $retarr;
				}
				
				$retarr = $body_obj->id;
			}
			return $retarr;
		}
		
		function getMyobDebitorFromItem(&$items){
			$debitor = 28439;
			
			foreach($items as $item){

				$product_id = $item['product_id'];
				
				$prod = wc_get_product($product_id);
				if(! $prod) continue;
				
				$sku = $prod->get_sku();
				if(! $sku) continue;
				
				if(stristr($sku, 'THOM') !== FALSE){
					$debitor = 28439;
				}elseif(stristr($sku, 'ERK') !== FALSE){
					$debitor = 28440;
				}elseif(stristr($sku, 'ROCK') !== FALSE){
					$debitor = 28441;
				}
				
				break;
			}
			
			return $debitor;
		}
		
		function create_order_via_api($debtorid, $order_id, $items, $total, $deliveryaddress){
			global $woocommerce;
			
			$line_items = array();
			$locid = 1;
			$branchid = 2;
			
			foreach($items as $item){
				$line_item = array();
				
				$qty = $item['qty'];
				$product_id = $item['product_id'];
				//$prod = new WC_Product($product_id);
				$prod = wc_get_product($product_id);
				if(! $prod) continue;
				
				if($prod->is_virtual()){
					$stocktype = "LookupItem";
				}else{
					$stocktype = "PhysicalItem";
				}
				
				$sku = $prod->get_sku();
				if(! $sku) continue;
				
				if(stristr($sku, 'THOM') !== FALSE){
					$locid = 1;
					$branchid = 2;
				}elseif(stristr($sku, 'ERK') !== FALSE){
					$locid = 3;
					$branchid = 4;
				}elseif(stristr($sku, 'ROCK') !== FALSE){
					$locid = 4;
					$branchid = 7;
				}
				
				$sku = str_ireplace('THOM-', '', $sku);
				$sku = str_ireplace('ERK-', '', $sku);
				$sku = str_ireplace('ROCK-', '', $sku);
				
				$line_item['stockcode'] = $sku;
				$line_item['orderquantity'] = $qty;
				$line_item['unitprice'] = round( ($item['line_total'] / $item['qty'] ), 2);
				$line_item['taxratevalue'] = $item['line_tax'];
				$line_item['linetotal'] = $item['line_total'];
				$line_item['linetype'] = 0;
				$line_item['description'] = $prod->get_title();
				$line_item['stocktype'] = $stocktype;
				$line_item['duedate'] = date('Y-m-d');
				$line_item['IsOriginatedFromTemplate'] = 'false';
				$line_item['discount'] = 0;
				$line_item['IsPriceOverridden'] = 'false';
				$line_item['LocationId'] = $locid;
				$line_item['defaultlocationid'] = $locid;
				//$line_item['branchid'] = $branchid;
				$line_item['TaxRateId'] = 1;
				$line_item['listprice'] = $line_item['unitprice'];
				$line_item['pricepolicyid'] = -1;
				$line_item['narrative'] = '';
				$line_item['BatchCode'] = '';
				$line_item['BranchId'] = $branchid;
				
				$line_items[] = $line_item;
				
			}
			
			$order = new WC_Order($order_id);
			
			/* $shipping = $order->get_items( 'shipping' );
			$ship_method = array_pop($shipping);
			$ship_id = $ship_method['method_id']; */
			
			
			$user_uri = "https://exo.api.myob.com";
			$url = $user_uri.'/salesorder/';
			
			$headers = array();
			$headers['Authorization'] = get_option( 'aspk_exo_password' );
			$headers['x-myobapi-key'] = get_option( 'aspk_exo_apiKey' );
			$headers['x-myobapi-exotoken'] = get_option( 'aspk_exo_accessToken' );
			$headers['Accept'] = 'application/json';
			$headers['Content-Type'] = 'application/json';
			
			
			$extra = array();
			
			/* if($ship_id == 'local_pickup'){
				
				$ship_via['key'] =  'X_SHIP_VIA';
				$ship_via['value'] =  'AUSTRALIA POST';
				
				$eparcel['key'] = 'X_COURIER_PRODUCT';
				$eparcel['value'] = 'X1';
				
				$extra[] = $ship_via;
				$extra[] = $eparcel;
				
			}else{ */
				
				$ship_via['key'] =  'X_SHIP_VIA';
				$ship_via['value'] =  'PICKUP';
				
				$extra[] = $ship_via;
				
			//}
			
			$body = array(
				'debtorid' => $debtorid,
				'lines' => $line_items,
				'status' => 0,
				'ordertotal' => $total,
				'narrative' => home_url(),
				'allowcustomerordernumber' => 'true',
				'reference' => '',
				'instructions' => '',
				'finalisationdate' => '',
				'activationdate' => '',
				'BranchId' => $branchid,
				'DefaultLocationId' => $locid,
				'ContactId' => -1,
				'SalesPersonId' => 1017,
				'ExchangeRate' => 1,
				'OrderDate' => date('Y-m-d'),
				'duedate' => date('Y-m-d'),
				'DeliveryAddress' => $deliveryaddress,
				//'extrafields' => $extra,
				'customerordernumber' => $order_id,
				'id' => 0,
				'currencyid' => 0
				
			);
						
			$args = array(
				'method' => 'POST',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => $headers,
				'body' => json_encode($body),
				'cookies' => array()
			);
			
			
			
			$response = wp_remote_post( $url, $args);
			
			
			
			$body = $response['body'];
			$body_obj = json_decode($body);
			
			
			
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   $order->add_order_note("Error:".$error_message);
			   return '0';
			   
			}else{
				$ret = $response['response'];
				if($ret['code'] != '201'){
					$order->add_order_note("Error:".$ret['message'] .$body_obj->message  );
					return '0';
				}
				
				$order->add_order_note("Order created on EXO with id ".$body_obj->id);
				
				update_post_meta( $order_id, '_dropbox_order_id', $body_obj->id);
			}
			
		}
		
		function get_user_from_api($firstname, $lastname){
			
			$customer_name = $firstname.' '.$lastname;
			$user222 = urlencode ( $customer_name );

			$host = "https://exo.api.myob.com/debtor/search?q=".$user222; 
			
			$authHash = get_option( 'aspk_exo_password' );
			
			$apiKey = get_option( 'aspk_exo_apiKey' );
			
			$accessToken = get_option( 'aspk_exo_accessToken' );
			
			$response = $this->sendPost($host,$authHash,$apiKey,$accessToken);
			
			if ( is_wp_error( $response ) ) {
				//$error_message = $response->get_error_message();
				return '0';
			}else{

				foreach($response as $obj){
					$acount_name = $obj->accountname;
					if( $customer_name == $acount_name ){
						return $obj->id;
					}
				}
				return '0';
			}
			
		}
		
		function woocommerce_product_query($q){
			
			if(isset($_SESSION['post_data'])){
				$json_array = json_decode($_SESSION['post_data']);
				$wh = $json_array->warehouse[0];
				
				if(! empty($wh)){
					$meta_query = $q->get( 'meta_query' );

					$meta_query[] = array(
								'key'       => '_sku',
								'value'   	=> $wh.'-',
								'compare'   => 'LIKE'
							);
					
					$q->set( 'meta_query', $meta_query );
				}
			}
			
		}

	
		
	}// class ends
	
}// if ends

new MyobExo();