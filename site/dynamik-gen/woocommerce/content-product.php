<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

if(isset($_SESSION['out_of_coverage_area'])){  ?>
	&nbsp;

	&nbsp;
	<p style="text-align: center;"><img class="aligncenter size-full wp-image-6122" src="/wp-content/uploads/2017/07/Up-Arrow.png" alt="Set Postcode" width="80" height="76" /></p>

	<h2 style="text-align: center;">SET YOUR DELIVERY<br>
	POSTCODE TO SEE<br>
	AVAILABLE PRODUCTS</h2>
	<p style="text-align: center;">If you have set your postcode we don't currently deliver to your area.<br>
	To enquire about a special delivery <a href="/contact/">contact us</a>.</p>
		<?	
		// do_action( 'woocommerce_sidebar' );
		
	do_action( 'woocommerce_after_main_content' );
	?>
	</div><!--site inner-->
	<?php
	get_footer( 'shop' );

		exit; 
	}

	
/* if(isset($_SESSION['post_data'])){

	$sku = $product->get_sku();
	$json_array = json_decode($_SESSION['post_data']);
	$wh = $json_array->warehouse;
	
	 foreach($wh as $k => $v) {
		 $whouse = $v;
	 }
	$my_wh = $whouse; 
	$_SESSION['aspk_wh'] = $my_wh;
	$str = substr($sku, 0, strlen($my_wh));
	if($str != $my_wh) return;
} */


if (!(isset($_SESSION['post_data']) )) { ?>
		
		&nbsp;

	&nbsp;
	<p style="text-align: center;"><img class="aligncenter size-full wp-image-6122" src="/wp-content/uploads/2017/07/Up-Arrow.png" alt="Set Postcode" width="80" height="76" /></p>

	<h2 style="text-align: center;">SET YOUR DELIVERY<br>
	POSTCODE TO SEE<br>
	AVAILABLE PRODUCTS</h2>
	<p style="text-align: center;">If you have set your postcode we don't currently deliver to your area.<br>
	To enquire about a special delivery <a href="/contact/">contact us</a>.</p>
		<?	
		// do_action( 'woocommerce_sidebar' );
		
	do_action( 'woocommerce_after_main_content' );
	
	?>
	</div><!--site inner-->
	<?php
	
	get_footer( 'shop' );
	
		exit;
		
		
	}
	
	
?>
<li <?php post_class(); ?>>
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	
	do_action( 'woocommerce_after_main_content' );
	
	?>
</li>
